# Definição #

* Projeto criado no formado SPA (single page application) para cadastrar/visualizar candidatos para vagas de emprego.

### Configs ###

* Projeto executado no framework php laravel, versão 8
* Componentes adicionais
* JQuery, boostrap, VueJS.
* Banco de dados postgreSQL

### Como iniciar a aplicação ###

* Configurar a base no arquivo .env (exemplo foi utilizado postgreSQL)
* Rodar as migrations para criar as tabelas (candidato_x_conhecimento,candidatos,conhecimentos, failed_jobs, migrations, password_resets, personal_access_tokens, users): php artisan migrate
* Rodar as seeder: php artisan db:seed (para criar o usuário admin e os conhecimentos)
* Iniciar a aplicação: php artisan serve (ou outra forma para subir o servidor)

### Como fazer login ###

* Usuário admin padrão cadastrado : email 'admin@admin.com' / senha '123'