<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Candidato;
use App\Models\Candidato_X_Conhecimento;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;

class CandidatosController extends Controller
{
    public function list(Request $request)
    {
        //return $request;
        $r = [];
        $count = 0;
        $dados =  $request->all();
        foreach ($dados['tecnologia'] as $key => $value) {
            if ($value) {
                $r[] = $dados['tecnologia_id'][$key];
                $count++;
            }
        }

        $request['count'] = $count;
        if ($count == 0) {
            $lista = Candidato::leftJoin('candidato_x_conhecimento', 'candidato_x_conhecimento.id_candidato', '=', 'candidatos.id')
                ->leftJoin('conhecimentos', 'conhecimentos.id', '=', 'candidato_x_conhecimento.id_conhecimento')
                ->select('candidatos.name', 'candidatos.email', 'candidatos.idade', 'candidatos.linkedin', DB::raw("string_agg(conhecimentos.tecnologia , ', ') as tecnologia"))
                ->groupBy('candidatos.name', 'candidatos.email', 'candidatos.idade', 'candidatos.linkedin')
                ->get();
            return response()->json([
                $lista
            ]);
        } else {
            $u = DB::table('conhecimentos')
                ->leftJoin('candidato_x_conhecimento', 'candidato_x_conhecimento.id_conhecimento', '=', 'conhecimentos.id')
                ->select('candidato_x_conhecimento.id_candidato')
                ->whereIn('conhecimentos.id', $r)
                ->get();

            foreach ($dados['tecnologia'] as $key => $value) {
                if ($value) {
                    $r[] = $dados['tecnologia_id'][$key];
                    $count++;
                }
            }

            $uf = [];
            for ($i=0; $i < count($u); $i++) { 
                $uf[] =$u[$i]->id_candidato;
            }
           
            $lista = Candidato::leftJoin('candidato_x_conhecimento', 'candidato_x_conhecimento.id_candidato', '=', 'candidatos.id')
                ->leftJoin('conhecimentos','conhecimentos.id', '=', 'candidato_x_conhecimento.id_conhecimento')
            	->select('candidatos.name','candidatos.email','candidatos.idade','candidatos.linkedin',DB::raw("string_agg(conhecimentos.tecnologia , ', ') as tecnologia"))
                ->whereIn('candidatos.id', $uf)
                ->groupBy('candidatos.name','candidatos.email','candidatos.idade','candidatos.linkedin')
            	->get();
            return response()->json([
                $lista
            ]);
        }
    }

    public function novo(Request $request)
    {
        $dados =  $request->all();
        if (($dados['name'] == "")) {
            throw ValidationException::withMessages([
                'mensagem' => ['preencha um nome para o candidato']
            ]);
        }

        if (($dados['email'] == "") || !filter_var($dados['email'], FILTER_VALIDATE_EMAIL)) {
            throw ValidationException::withMessages([
                'mensagem' => ['preencha um email válido ']
            ]);
        }
        //valida se email já existe
        $id = DB::table('candidatos')->where('email', $dados['email'])->value('id');
        if (is_int($id)) {
            throw ValidationException::withMessages([
                'mensagem' => ['e-mail já cadastrado']
            ]);
        }
        if (($dados['idade'] <= 0)) {
            throw ValidationException::withMessages([
                'mensagem' => ['preencha uma idade válida']
            ]);
        }

        $candidato = new Candidato();
        $candidato->name = $dados['name'];
        $candidato->email = $dados['email'];
        $candidato->idade = $dados['idade'];
        $candidato->linkedin = $dados['linkedin'] == null ? "" : $dados['linkedin'];


        $count = 0;
        if ($candidato->save()) {
            foreach ($dados['tecnologia'] as $key => $value) {
                if ($value) {
                    $cc = new Candidato_X_Conhecimento();
                    $cc->id_conhecimento = $dados['tecnologia_id'][$key];
                    $cc->id_candidato = $candidato->id;
                    $cc->save();
                    $count++;
                }
            }
            if ($count == 0) {
                $cc = new Candidato_X_Conhecimento();
                $cc->id_conhecimento = 0;
                $cc->id_candidato = $candidato->id;
                $cc->save();
                $count++;
            }

            return response()->json($candidato, 200);
        }
        throw ValidationException::withMessages([
            'mensagem' => ['Erro ao cadastrar novo candidato']
        ]);
    }

    public function count()
    {
        return response()->json(DB::table('candidatos')->count(), 200);
    }
}
