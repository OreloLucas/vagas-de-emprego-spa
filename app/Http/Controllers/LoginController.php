<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Laravel\Sanctum\HasApiTokens;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $dados = $request->all();
        if (($dados['email'] == "") || ($dados['password'] == "") ){
            throw ValidationException::withMessages([
                'mensagem' =>['Dados em brancos']
            ]);
        }

        if (!filter_var($dados['email'], FILTER_VALIDATE_EMAIL)) {
            throw ValidationException::withMessages([
                'mensagem' =>['Login precisa ser um email válido']
            ]);
        }
        
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            //$request->session()->regenerate();
            //session(['auth' => true]);
            return response()->json(Auth::user(), 200);
        }
        throw ValidationException::withMessages([
            'mensagem' =>['e-mail/ senha incorretos']
        ]);
    }

    public function logout(Request $request)
    {
                session(['auth' => false]);
        

    }
}
