<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidato_X_Conhecimento extends Model
{
    use HasFactory;

    protected $table = 'candidato_x_conhecimento';
    protected $fillable = [
        'id_candidato',
        'id_conhecimento',
    ];
}
