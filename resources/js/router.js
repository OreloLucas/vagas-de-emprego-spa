
import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from './pages/Home.vue';
import About from './pages/About.vue';
import Login from './pages/Login.vue';
import Dashboard from './pages/Dashboard.vue';
import Candidatos from './pages/Candidatos.vue';
import Candidato_novo from './pages/Candidato_novo.vue';

Vue.use(VueRouter);
var logged = false;
const router = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'about',
            component: About
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: "/dashboard",
            name: "Dashboard",
            component: Dashboard,
            beforeEnter: (to, form, next) =>{
                if (localStorage.auth == 'true'){
                    next();
                }else{
                    return next({ name: 'Login'});  
                }
           },       
        },
          {
            path: "/candidatos",
            name: "Candidatos",
            component: Candidatos,
            beforeEnter: (to, form, next) =>{
                if (localStorage.auth == 'true'){
                    next();
                }else{
                    return next({ name: 'Login'});  
                }
            },       
          },
          {
            path: "/candidato_novo",
            name: "Candidato_novo",
            component: Candidato_novo,
            beforeEnter: (to, form, next) =>{
                if (localStorage.auth == 'true'){
                    next();
                }else{
                    return next({ name: 'Login'});  
                }
            },       
          }
    ]
});

export default router;
