<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'LoginController@login');
Route::post('/logout', 'LoginController@logout');

Route::post('/candidato-lista', 'CandidatosController@list');
Route::post('/candidato-novo', 'CandidatosController@novo');
Route::post('/candidato-count', 'CandidatosController@count');
