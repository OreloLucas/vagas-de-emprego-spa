<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Conhecimento;
use Illuminate\Support\Facades\DB;

class ConhecimentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tec = ['Nenhum', 'C#', 'Javascript', 'Nodejs', 'Angular', 'React', 'Ionic', 'Mensageria', 'PHP', 'Laravel'];

        // DB::table('conhecimentos')->delete();
        $conhecimento = new Conhecimento();
        
        for ($i=0; $i < count($tec) ; $i++) { 
            $id = DB::table('conhecimentos')->where('tecnologia', $tec[$i])->value('id');
            if (!is_int($id)){
                $conhecimento = new Conhecimento();
                $conhecimento->id= $i;
                $conhecimento->tecnologia=  $tec[$i];
                $conhecimento->save();  
            }
        }
    }
}
