<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class userSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuario = User::find(1);
        
        if ($usuario == null){
            $usuario = new User();
            $usuario->id=1;
            $usuario->name="admin";
            $usuario->email="admin@admin.com";
            $usuario->password=bcrypt("123");
            $usuario->save();
        }

    }
}
